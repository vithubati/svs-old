'''
Created on Apr 03, 2016

@author: vithu
'''

import numpy as np

# Centroids
WHITE_CENTROID = [255, 255, 255]
BLACK_CENTROID = [0, 0, 0]

def kmeans(pixels, k_clusters):
    """ k-Means clustering algorithm specially built for SVS
    pixels = set of Pixels
    k = number of clusters
    """
    centroids = []
    # initial Centroids
    centroids = [WHITE_CENTROID, BLACK_CENTROID]  
    prev_centroids = [[] for i in range(k_clusters)] 
    
    iterations = 0
    while not (hasConverged(centroids, prev_centroids, iterations)):
        iterations += 1
        clusters = [[] for i in range(k_clusters)]
        # assign data points to clusters
        clusters = getEuclideanDist(pixels, centroids, clusters)
        # recalculate centroids
        index = 0
        for cluster in clusters:
            prev_centroids[index] = centroids[index]
            centroids[index] = np.mean(cluster, axis=0).tolist()
            index += 1

    WhitePix = len(clusters[0])
    BlackPix = len(clusters[1])
          
    return (WhitePix, BlackPix)

   
def hasConverged(centroids, prev_centroids, iterations):
    """ hasConverged
    Returns True or False if k-means is done. K-means terminates either
    because it has run a maximum number of iterations OR the centroids
    stop changing (CONVERGED). 
    """
    # Maximum number of iterations of the k-means algorithm for a single run
    max_iter = 1000 
    if iterations > max_iter:
        return True
    return prev_centroids == centroids

   
def getEuclideanDist(pixels, centroids, clusters):
    """ getEuclideanDist
    Calculates euclidean distance between a pixel and each
    cluster centroid   
    """
    for instance in pixels:  
        # Find closest centroid to the pixels point.
        label = min([(i[0], np.linalg.norm(instance-centroids[i[0]])) \
                            for i in enumerate(centroids)], key=lambda t:t[1])[0]
        try:
            clusters[label].append(instance)
        except KeyError:
            clusters[label] = [instance]
            
    for cluster in clusters:
        if not cluster:
            cluster.append(pixels[np.random.randint(0, len(pixels), size=1)].flatten().tolist())
                        
    return clusters



