import cv2
import time
#from Project import YawnDetection
from YawnDetection2 import YawnDetection2
from IrisDetectionB import IrisDetectionB
import Vigilant
class Main: 
    face_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_frontalface_alt.xml')
    eye_cascade = cv2.CascadeClassifier('../res/haarcascades/haarcascade_eye_tree_eyeglasses.xml')
    
    fileName = '../res/MyMovie6.mp4'
    fileName2 = '../res/MyMovie22.mp4'
    fileName3 = '../res/MyMovie11.mp4'
    fileName4 = '../res/yawn3.mp4'
    def __init__(self):
        pass
    
    def initialize(self):
        cap= None
        irisList=[]
        yawnList=[]
        start_time = time.time()
        try:
            cap = cv2.VideoCapture(self.fileName3)  
#             fps = cap.get(cv2.CAP_PROP_FPS)
#             print "Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps)

            noOfFrames = 0
            print "system start"
            while 1:
                try:
                    cv2.waitKey(1)
                    yawndetected2 = False
                    irisDetected = False
                    if cap.isOpened():
                        #print 'captured'
                        # Take each frame
                        captured, frame = cap.read()
                        if captured:
                            # grab the dimensions of the image and calculate the center of the image
                            #(h, w) = frame.shape[:2]
                            #center = (w / 2, h / 2)
                             
                            # rotate the image by 180 degrees
                            #M = cv2.getRotationMatrix2D(center, 270, 0.5)
                            #frame = cv2.warpAffine(frame, M, (w, h))
                            gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
                            faces = self.face_cascade.detectMultiScale(gray, 1.3, 5)  # returns Rect(x,y,w,h)
                            if len(faces) >0:
                                #print 'face len', len(faces)
                                for (x, y, w, h) in faces: 
                                    #frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                                    roi_gray = gray[y:y + h, x:x + w]  # gray face ROI
                                    roi_color = frame[y:y + h, x:x + w]  # color Face ROI
                                    yawnDet = YawnDetection2()
                                    irisDet = IrisDetectionB()
                                    #yawndetected = yawnDet.detectYawning(roi_gray)                                    
                                    irisDetected = irisDet.detectIris(roi_color, roi_gray)
                                    yawndetected2 = yawnDet.detectYawning(roi_gray)
                                    if noOfFrames < 81:
                                        irisList.append(irisDetected)
                                        yawnList.append(yawndetected2) 
                                        noOfFrames +=1
                                    else:
                                        decision = self.makeDecision(irisList, yawnList)
                                        self.alertUser(decision)
                                        #print "Finished"
                                        noOfFrames = 0
                                        irisList = []
                                        yawnList = []
                                        #print("--- %s seconds ---" % (time.time() - start_time))
                                        #start_time = time.time()
                                        break
                                    #print "Yawning ", yawndetected2
                                    #print "Eye Open", irisDetected
                                    cv2.imshow('dd',frame)
                                    if cv2.waitKey(5) & 0xFF == ord('q'):
                                        break  
                            else:
                                #print "No Face Detected"        
                                pass    
                        else:
                            print "No Capture"
                            break             
                    else:
                        print "No Opened" 
                except:
                    break
        except:
            print "SYSTEM STOPPED"
            pass 
        finally:
            cap.release()     

    def makeDecision(self, irisList, yawnList):
        alert = "STABLE"
        try:
            alert = "STABLE"
            if irisList.count(False) > 60:
                print 'DANGER'
                alert = "DANGER"
            elif irisList.count(False) > 50:
                print 'SLEEPY'
                alert = "SLEEPY"
            elif yawnList.count(True) > 30:
                print 'FATIGUE'
                alert = "FATIGUE"
            return alert
        except:
            return alert
            pass
        
    def alertUser(self, message):
        try:
            if message is 'DANGER':
                Vigilant.speak("Sir!! Wake up sir. sir!! Wake up!! Sir!! There is a chance for a collision to be happened. I kindly request you to stop the vehicle and have some rest please")
                #print 'DANGER' 
            elif message is 'FATIGUE':
                Vigilant.speak("Sir? It seems You are exhausted. Would you mind having some Rest please?")
                #print 'FATIGUE'
            return
        except:
            return                    
init = Main()
init.initialize()  
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    